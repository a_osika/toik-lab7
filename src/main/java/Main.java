import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    private static String FILE = "cv.pdf";

    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addTitle(document, "Resume");
            addTable(document);
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void addTitle(Document document, String value)
            throws DocumentException {
        Paragraph title = new Paragraph(new Phrase(10f, value, FontFactory.getFont(FontFactory.HELVETICA, 28f)));
        title.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(title, 5);
        document.add(title);
    }

    private static void addTable(Document document) throws DocumentException {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(80);
        table.setSpacingBefore(20f);
        table.setSpacingAfter(10f);

        table.addCell("First name");
        table.addCell("Andrzej");
        table.addCell("Last name");
        table.addCell("Osika");
        table.addCell("Profession");
        table.addCell("Student");
        table.addCell("Education");
        table.addCell("2018-now PWSZ Tarnow");
        table.addCell("Summary");
        table.addCell("Learning to code");

        document.add(table);
    }
}
